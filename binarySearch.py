import random
numbers = [791, 916, 1426, 2440, 3616, 5063, 6834, 12695, 18175, 19265, 25378, 37341, 68569, 137052, 140829, 232510, 270996, 300646, 353384, 447891, 895763, 1639080, 1969390, 2372135, 4299830, 7402651, 13199768, 21131760, 41407962, 69620784, 85292840, 130899014, 201449649, 386535459, 558176349, 831048756, 1263069983, 2322619054, 4109940319, 6194388182, 6937911820, 9464802205, 18278729881, 35419100551, 47594499708, 79302869281, 157283974797, 172616561109, 187693884881, 327649096714];
numberToFindIndex = random.randint(0,len(numbers)-1)
numberToFind = numbers[numberToFindIndex]
print("Looking for: "+str(numberToFind)+"["+str(numberToFindIndex)+"] in "+str(numbers)+"\n\n")
currentNumberIndex = 0
currentNumber = numbers[currentNumberIndex]
max = len(numbers)
maxValue = numbers[max-1]
min = 0
minValue = numbers[min]
i = 0

def printNumbersInRange():
    global numbers,max,min
    textToDisplay = "Narrowed down to: "
    for i in range(max-min):
        textToDisplay+=str(numbers[i+min])+", "
    print(textToDisplay)

def printData():
    global numbers,numberToFind,currentNumber,currentNumberIndex,max,maxValue,min,minValue
    printNumbersInRange()
    print("numberToFind:"+str(numberToFind))
    print("currentNumber:"+str(currentNumber))
    print("currentNumberIndex:"+str(currentNumberIndex))
    print("max:"+str(max))
    print("maxValue:"+str(maxValue))
    print("min:"+str(min))
    print("minValue:"+str(minValue)+"\n\n")

def step():
    global numbers,numberToFind,currentNumber,currentNumberIndex,max,min
    if currentNumber < numberToFind:
        min = currentNumberIndex
        currentNumberIndex = int((max+min)/2)
        currentNumber = numbers[int((max+min)/2)]
    elif currentNumber > numberToFind:
        max = currentNumberIndex
        currentNumberIndex = int((max+min)/2)
        currentNumber = numbers[int((max+min)/2)]
    
    printData()
    
while currentNumber != numberToFind and i < 100:
    i+=1
    step()
if currentNumber == numberToFind:
    print("Done! Found number: "+str(numberToFind)+"["+str(numberToFindIndex)+"] in "+str(numbers)+"\n\nTook: "+str(i)+" cycles of the binary search algorithm.")
else:
    print("Took to long, stopping...");