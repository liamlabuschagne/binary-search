import random
import sys
def gen(leng):
    nums = []
    for i in range(leng):
        if len(nums) == 0:
            nums.append(random.randint(0,1000))
        else:
            nums.append(random.randint(nums[-1]+1,(nums[-1]+1)*2))
    print(nums)    
if len(sys.argv) != 2:
    print("No length specified.")
elif not sys.argv[1].isdigit() or int(sys.argv[1]) < 1:
    print("Value passed not a valid number.")
else:
    gen(int(sys.argv[1]))